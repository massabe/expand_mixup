# Expand mixup project
This repository contains the experimentation code for my final year research project at AIMS Senegal. It consists of using a combination of **SMOTE** and a new technique called **mixup** with a very  imbalanced credit card transactions dataset to predict either it is fraudulant or not.

# Dataset
This dataset contains the European credit card transactions that occurred over two days. It has 284, 807 rows, with 284, 315 normal and 492 fraudulant transactions, and 31 columns downloadable from [here](https://www.kaggle.com/datasets)
